# Desafio Pessoa Desenvolvedora Java

<h1 align="center">
    Teste: Filmes API
  <p><img src="https://img.shields.io/badge/Java-ED8B00?style=for-the-badge&logo=java&logoColor=white"/> 
  <img src="https://img.shields.io/badge/Eclipse-2C2255?style=for-the-badge&logo=eclipse&logoColor=white"/>
  <img src ="https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white"/>
  <img src="https://img.shields.io/badge/-Swagger-%23Clojure?style=for-the-badge&logo=swagger&logoColor=white">
  <img src="https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white"></p>
  
  
</h1>

-----
# 🚨 Requisitos

- A API deve ser construída em Java (8 ou superior) utilizando Spring Framework (2.2 ou superior)
- Implementar autenticação seguindo o padrão ***JWT***, lembrando que o token a ser recebido deve estar no formado ***Bearer***
- Implementar operações no banco de dados utilizando ***Spring Data JPA*** & ***Hibernate***
- **Bancos relacionais permitidos**
    - *MySQL* (prioritariamente)
    - *PostgreSQL*
- As entidades deversão ser criadas como tabelas utilizando a ferramenta de migração **Flyway**. Portanto, os scripts de **migrations** para geração das tabelas devem ser enviados no teste
- Sua API deverá seguir os padrões REST na construção das rotas e retornos
- Sua API deverá conter documentação viva utilizando a *OpenAPI Specification* (**Swagger**)
- Caso haja alguma particularidade de implementação, instruções para execução do projeto deverão ser enviadas.

📚 Features
-----

## Usuario
  
  - [x] LIST
  - [x] CREATE


## Avaliacao
  - [x] CREATE
  - [x] LIST
  - [x] UPDATE

  
## Pesquisar Filmes
  - [x] LIST
  - [x] PESQUISAR POR TITULO
  - [x] PESQUISAR POR GENERO
  - [x] PESQUISAR POR DIRETOR
  - [x] UPDATE
  - [x] CREATE
  - [x] DELETE

# 🖥 Testes
- Testes Unitarios - JUNIT.


💻 Configurações
-----

- Swagger ``http://localhost:8080/swagger-ui/``
- usuario: root senha: root

---

## 📝 Implementações que faltam


  - [x] Usuario Admin e seus complementos
  - [x] Exclusão lógica (desativação)
  - [x] Voto (a contagem de votos de 0-4)
  - [x] Retornar a lista ordenada por filmes mais votados e por ordem alfabética