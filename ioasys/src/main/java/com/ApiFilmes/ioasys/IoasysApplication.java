package com.ApiFilmes.ioasys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IoasysApplication {

	public static void main(String[] args) {
		SpringApplication.run(IoasysApplication.class, args);
	}

}
