package com.ApiFilmes.ioasys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ApiFilmes.ioasys.model.Filme;
import com.ApiFilmes.ioasys.repository.FilmeRepository;

@RestController
@RequestMapping("/filmes")
@CrossOrigin("*")
public class FilmeController {
	
	@Autowired
    private FilmeRepository repository;
	@GetMapping
	public ResponseEntity <List<Filme>> GetAll(){
		return ResponseEntity.ok(repository.findAll());
	}
	
	@GetMapping ("/{id}")
	public ResponseEntity <Filme> GetById(@PathVariable long id){
		return repository.findById(id)
				.map(resp -> ResponseEntity.ok (resp))
				.orElse(ResponseEntity.notFound().build());
	}
	@GetMapping ("/titulo/{titulo}")
	public ResponseEntity <List<Filme>> GetByTitulo(@PathVariable String titulo){
		return ResponseEntity.ok(repository.findAllByTituloContainingIgnoreCase(titulo));
	}
	
	@GetMapping ("/genero/{genero}")
	public ResponseEntity <List<Filme>> GetByGenero(@PathVariable String genero){
		return ResponseEntity.ok(repository.findAllByGeneroContainingIgnoreCase(genero));
	}
	
	@GetMapping ("/diretor/{diretor}")
	public ResponseEntity <List<Filme>> GetByDiretor(@PathVariable String diretor){
		return ResponseEntity.ok(repository.findAllByDiretorContainingIgnoreCase(diretor));
	}
	
	@PostMapping
	public ResponseEntity<Filme> post (@RequestBody Filme filme){
		return  ResponseEntity.status(HttpStatus.CREATED).body(repository.save(filme));
	}
	
	@PutMapping
	public ResponseEntity<Filme> put (@RequestBody Filme filme){
		return  ResponseEntity.status(HttpStatus.OK).body(repository.save(filme));
	}
	
	@DeleteMapping("/{id}")
	public void delete (@PathVariable long id) {
		repository.deleteById(id);
	} 
	
}
