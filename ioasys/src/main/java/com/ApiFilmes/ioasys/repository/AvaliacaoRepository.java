package com.ApiFilmes.ioasys.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ApiFilmes.ioasys.model.Avaliacao;

public interface AvaliacaoRepository extends JpaRepository <Avaliacao, Long> {

	public List<Avaliacao> findAllByDescricaoContainingIgnoreCase(String descricao);
}
